import {BrainyConfigRouter} from 'brainy-config-router';

describe('ConfigRouter', () => {
  let brainyConfigRouter;
  let configToHandle;

  beforeEach(() => {
    configToHandle = {
      map: function(routes) {
        this.routes = routes;
      }
    };
    brainyConfigRouter = new BrainyConfigRouter();
    brainyConfigRouter.manage('a title', configToHandle);
  });

  it('should have a title set', () => {
    expect(configToHandle.title).toBe('a title');
  });

  it('should route to config when no url is defined', () => {
    const firstRoute = configToHandle.routes[0];
    expect(firstRoute.route[0]).toBe('');
    expect(firstRoute.name).toBe('config');
    expect(firstRoute.moduleId).toBe('./config/config');
    expect(firstRoute.nav).toBe(true);
    expect(firstRoute.title).toBe('Config');
  });
});
