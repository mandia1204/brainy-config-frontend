export class BrainyConfigRouter {
  manage(title, configToHandle) {
    configToHandle.title = title;
    this.handleRoutes(configToHandle);
  }

  handleRoutes(config) {
    config.map([
      { route: ['','config'], name: 'config', moduleId: './config/config', nav: true, title:'Config' }
    ]);
  }
}
